const {createServer} = require('http');

createServer((req, res) => res.end('Hello World!'))
    .listen(8080, () => console.log('Listening on 8080'));