FROM node:14

WORKDIR /home/node/app
ADD *.json *.js ./

USER node
CMD ["npm", "start"]
EXPOSE 8080